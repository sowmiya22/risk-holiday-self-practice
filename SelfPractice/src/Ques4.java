package self.practice.java;

public class Ques4 {
	

		static final String pattern = "/";
		static final int designPerLine = 12;
		static final int numberofLines = 10;

		public static void printSlashes() {

			for (int i = 1; i <= designPerLine; i++) {
				System.out.print(pattern + " ");
			}
			System.out.println();
			for (int i = 1; i < designPerLine; i++) {
				System.out.print(" " + pattern);
			}
			System.out.println();
		}
		
		public static void printPattern() {
			for (int i = 0; i < numberofLines/2; i++) {
				Ques4.printSlashes();
			}
		}
		
		public static void main(String[] args) {
			Ques4.printPattern();
		}

	}

