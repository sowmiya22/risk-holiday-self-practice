package self.ques23.java;
public class WordString {
public static void main(String[] args) {
		
		String word = "School.of.Progressive. Rock";
	
			System.out.println("A : " + word.length());
			System.out.println("B : " + word.substring(22));
			System.out.println("C : " + word.substring(22, 24));
			System.out.println("D : " + word.indexOf("oo"));
			System.out.println("E : " + word.toUpperCase());
			System.out.println("F : " + word.lastIndexOf("o"));
			System.out.println("G : " + word.indexOf("ok"));

	}
}

/*
A : 27
B :  Rock
C :  R
D : 3
E : SCHOOL.OF.PROGRESSIVE. ROCK
F : 24
G : -1

*/
  