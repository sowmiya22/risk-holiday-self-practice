package self.ques27.java;
public class Conceptcheck {
public static void main(String[] args) {
		
		String s = "Mississippi";
	
			System.out.println("a : "+s.length());
			
			System.out.println("b : "+s.indexOf( "si" ));
			
			System.out.println("c : "+s.toUpperCase().indexOf( "si" ));
			
			System.out.println("d : "+s.toLowerCase().indexOf( "si" ));
			
			System.out.println("e : "+s.substring(0,s.indexOf( "i" )));
			
			System.out.println("f : "+s.substring( s.lastIndexOf( "i" ) ));

	}
}

/*
 a : 11
b : 3
c : -1
d : 3
e : M
f : i
*/
