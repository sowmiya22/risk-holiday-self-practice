package self.ques21.java;

import java.util.Scanner;

public class RecieveAndPrint {
		public static void main(String args[]) {
				Scanner scan = new Scanner(System.in);
				
				System.out.print("ENTER STRING :");
				String s = scan.nextLine();
				System.out.print("ENTER INTEGER : ");
				int m = scan.nextInt();
				System.out.print("ENTER DOUBLE : ");
				double d = scan.nextDouble();
				
				System.out.println("S: " +s);
				System.out.println("M: " +m);
				System.out.println("D: " +d);
				System.out.println("M+D+S: " +m+d+s);
				System.out.println("M+S+D: " +m+s+d);
				System.out.println("S+M+D: " +s+m+d);
			}
		}

/*     
ENTER STRING :sowmiya
ENTER INTEGER : 5
ENTER DOUBLE : 2.5
S: sowmiya
M: 5
D: 2.5
M+D+S: 52.5sowmiya
M+S+D: 5sowmiya2.5
S+M+D: sowmiya52.5
*/
 
