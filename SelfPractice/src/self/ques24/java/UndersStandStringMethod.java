package self.ques24.java;
public class UndersStandStringMethod {
public static void main(String[] args) {
		
		String w = "Singin胆in_the_rain";;
		String pat = "in";
	
			System.out.println("1  : "+w.indexOf(pat));
			System.out.println("2  : "+w.indexOf(pat,3));
			System.out.println("3  : "+w.indexOf(pat,6));
			System.out.println("4  : "+w.lastIndexOf(pat));
			System.out.println("5  : "+w.length());
			System.out.println("6  : "+w.toUpperCase());
			System.out.println("7  : "+w.charAt(0));

}
}
/*  
1  : 1
2  : 4
3  : 7
4  : 16
5  : 18
6  : SINGIN?IN_THE_RAIN
7  : S
*/