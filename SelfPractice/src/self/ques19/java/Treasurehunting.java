package self.ques19.java;

	public class Treasurehunting {

		static final int treasureLocation = 40;

		private static boolean findTresure(int location, int preLocation) {

			if (location == treasureLocation) {
				System.out.println("You have found the treasure!");
				return true;
			} else {
				programAnnounces(location);
			}
			return false;
		}

		private static String programAnnounces(int location) {

			int distance = Math.abs(treasureLocation - location);

			if (distance >= 1 && distance <= 3) {
				return "The treasure is very close.";
			} else if (distance >= 4 && distance <= 6) {
				return "The treasure is somewhat close.";
			} else if (distance > 6) {
				return "The treasure is not close.";
			}
			return "";
		}

		public static void main(String[] args) {

			int guess = 0;
			do {

				int curLocation = self.ques30.java.UserInput.readNumber("Enter the treasure location");
				int preLocation = curLocation;
				if (findTresure(curLocation, preLocation)) {
					break;
				}
				guess++;
			} while (guess < 10);

		}
	}


