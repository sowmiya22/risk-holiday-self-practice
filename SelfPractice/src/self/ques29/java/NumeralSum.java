package self.ques29.java;
	import java.util.Scanner;
	public class NumeralSum {
	
		
		public static void main(String[] args) {
			
			Scanner scan = new Scanner(System.in);
			System.out.print("Enter a text : ");
			String word = scan.nextLine();
			
			int i = 0;
			
			for (int num = 0; num < word.length(); num++) {
				if (Character.isDigit(word.charAt(num))) {
					i = i + Character.getNumericValue(word.charAt(num));
				}
			}
			System.out.println("Total : "+ i);
		}
	}


