package self.ques28.java;

	import java.util.Scanner;
	public class Characterorderreversal {
	
		
		public static void main(String[] args) {
			
			Scanner scan = new Scanner(System.in);
			System.out.print("Enter an input String : ");
			
			String s = scan.nextLine();
			String reverse = " ";
			
			int length = s.length();
			for (int i = length - 1; i >= 0; i--) {
				reverse = reverse + s.charAt(i);
			}
			System.out.println("The reverse of "  + s +  " is "  + reverse + " ");
		}
	}

/*
Enter an input String : computer-programming
The reverse of computer-programming is  gnimmargorp-retupmoc 

*/
