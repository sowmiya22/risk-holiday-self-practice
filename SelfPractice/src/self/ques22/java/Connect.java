package self.ques22.java;
import java.util.Scanner;
public class Connect {
	

		public static void main(String[] args) {
			
				Scanner scan = new Scanner(System.in);
				
				System.out.print("Input W1 : ");
				String word1 = scan.nextLine();
				System.out.print("Input W2 : ");
				String word2 = scan.nextLine();

				char fcw1=word1.charAt(0);
				char fcw2=word2.charAt(0);
				char lcw1=word1.charAt(word1.length()-1);
				char lcw2=word2.charAt(word2.length()-1);
				System.out.print("Output : " + connect(word1,word2,fcw1,fcw2,lcw1,lcw2));
			}

			public static boolean connect(String word1, String word2,char fcw1,char fcw2,char lcw1,char lcw2) {
				return (lcw1==fcw2 || lcw2==fcw1 || (word1.length()==word2.length()));
			}
		}
/*
Input W1 : Agee
Input W2 : Sowmi
Output : false
*/
//
/*
Input W1 : pink
Input W2 : blue
Output : true
*/