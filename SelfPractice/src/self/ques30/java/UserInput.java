package self.ques30.java;

public class UserInput {
	public static class CyclicShift {
		
		public static String ShiftString(String text, int shift) {

			if (shift == 0 || shift == text.length()) {
				return text;
			}
			String prefix = text.substring(0, shift);
			String postfix = text.substring(shift);
			return postfix + prefix;
		}

		public void main(String[] args) {
			String w = UserInput.readText("Enter your input line");
			int k = UserInput.readNumber("Enter the shift value k");

			if (k >= 0 && k <= w.length()) {

				switch (k) {
				case 1:
					System.out.println("\nThe 1-st cyclic shift of");
					break;
				case 2:
					System.out.println("\nThe 2-nd cyclic shift of");
					break;
				case 3:
					System.out.println("\nThe 3-rd cyclic shift of");
					break;
				default:
					System.out.println("\nThe " + k + "-th cyclic shift of");
				}

				System.out.println("\"" + w + "\"\nis");
				System.out.println("\"" + ShiftString(w, k) + "\"");

			} else {
				System.out.println("\nvalue is invalid and stop");
			}

		}
	}

	public static String readText(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	public static int readNumber(String string) {
		// TODO Auto-generated method stub
		return 0;
	}
}
